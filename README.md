# docker

Hallo zusammen!

Ich bin Philipp und leidenschaftlicher It-ler.

In diesem Repository sind meine persönlichen Konfigurationsdateien zu finden. Hier teile ich meine Einstellungen und Anpassungen für meine Docker Container. Checkt es gerne mal aus! 🖥️🔧

> ⚠️ Hey Leute, nur 'ne kleine Info: Produkte können sich im Laufe der Zeit ändern. Ich geb mein Bestes, immer auf dem neuesten Stand zu sein, aber manchmal klappt das nicht ganz. Hoffe, das ist cool für euch! 😎

Ich hab' diese Ressourcen kostenlos für euch erstellt, damit ihr sie in euren speziellen Anwendungsfällen nutzen könnt. Falls ihr ausführliche Anleitungen zu bestimmten Tools oder Technologien sucht, schaut einfach mal auf meinem [YouTube-Kanal](https://www.youtube.com/@NetworkStudent) vorbei! 🎥🚀

---

Hello everyone!

I am Philipp and a passionate It-ler.

In this repository you can find my personal configuration files. Here I share my settings and customisations for my Docker containers. Feel free to check it out! 🖥️🔧

> ⚠️ Hey guys, just a little info: products can change over time. I do my best to always be up to date, but sometimes it doesn't quite work out. Hope that's cool for you!  😎

I've created these resources for you for free so you can use them in your specific use cases. If you're looking for in-depth tutorials on specific tools or technologies, check out my [YouTube-Channel](https://www.youtube.com/@NetworkStudent)! 🎥🚀
